import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter UI Controls',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Profil Sederhana'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String _dropdownButtonValue = 'One';
  String _popupMenuButtonValue = 'One';
  bool _checkboxValue = true;
  String _radioBoxValue = 'One';
  double _sliderValue = 10;
  bool _switchValue = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 10, right: 10),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              // TextField
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _GroupText('Nama'),
                  TextField(
                    obscureText: false,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Nama',
                    ),
                  ),
                  _GroupText('Tempat Tanggal Lahir'),
                  TextField(
                    obscureText: false,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Tempat Tanggal Lahir',
                    ),
                  ),
                  _GroupText('Alamat'),
                  TextField(
                    obscureText: false,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Alamat',
                    ),
                  ),
                  _GroupText('No. Hp'),
                  TextField(
                    obscureText: false,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'No. Hp',
                    ),
                  ),
                ],
              ),
              _SpaceLine(),
              // Radio[Box]
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _GroupText('Jenis Kelamin'),
                  RadioListTile(
                      title: const Text('Laki-Laki'),
                      value: 'Laki-Laki',
                      groupValue: _radioBoxValue,
                      onChanged: (value) {
                        setState(() {
                          _radioBoxValue = value;
                        });
                        _showToast('Changed value of Radio[Box] to $value');
                      }),
                  RadioListTile(
                      title: const Text('Perempuan'),
                      value: 'Perempuan',
                      groupValue: _radioBoxValue,
                      onChanged: (value) {
                        setState(() {
                          _radioBoxValue = value;
                        });
                        _showToast('Changed value of Radio[Box] to $value');
                      }),
                ],
              ),
              _SpaceLine(),
              // Checkbox
              Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                _GroupText('Makanan Favorit'),
                Row(
                  children: <Widget>[
                    Text('Nasi Goreng'),
                    Checkbox(
                        value: _checkboxValue,
                        onChanged: (newValue) {
                          setState(() {
                            _checkboxValue = newValue;
                          });
                          _showToast('Changed value of checkbox to $_checkboxValue');
                        }),
                  ],
                )
              ]),
              Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                Row(
                  children: <Widget>[
                    Text('Mie Pangsit'),
                    Checkbox(
                        value: _checkboxValue,
                        onChanged: (newValue) {
                          setState(() {
                            _checkboxValue = newValue;
                          });
                          _showToast('Changed value of checkbox to $_checkboxValue');
                        }),
                  ],
                )
              ]),
              SizedBox(
                height: 50,
              )
            ],
          ),
        ),
      ),
    );
  }

  void _showToast(String text) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(text),
      duration: Duration(seconds: 1),
    ));
  }
}

class _GroupText extends StatelessWidget {
  final String text;

  const _GroupText(this.text);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
      child: Text(
        text,
        style: TextStyle(fontSize: 25, fontWeight: FontWeight.w500),
      ),
    );
  }
}

class _SpaceLine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 5,
      child: Container(
        color: Colors.grey,
      ),
    );
  }
}
